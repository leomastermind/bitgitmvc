﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BitBucketMvc.Controllers
{
    public class LoadManagerController : Controller
    {
        //
        // GET: /LoadManager/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /LoadManager/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /LoadManager/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /LoadManager/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /LoadManager/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /LoadManager/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /LoadManager/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /LoadManager/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
